import React, { Component } from 'react';
import Bars from '../common/bar/bar';
import axios from 'axios'

class CollaborationBar extends Component {
    state = {  
         bars: []
    }
    
    Month = (BarID) =>{
        const bars = this.state.bars.slice()
        if (BarID === 1){
            bars[0].data = [...this.state.bars[0].dataM]
            this.setState({bars}) 
        }
        else if (BarID === 2){
            bars[1].data = [...this.state.bars[1].dataM]
            this.setState({bars}) 
       }
    }
    Year = (BarID) =>{
        const bars = this.state.bars.slice()
        if ( BarID === 1){
            bars[0].data = [...this.state.bars[0].dataY]
            this.setState({bars}) 
        }
        else{
            bars[1].data = [...this.state.bars[1].dataY]
            this.setState({bars}) 
       }

    }
    getAPI = () => {
        axios.get('http://localhost:3009/collaborationBar')
        .then(res => {  
            this.setState({
                bars : res.data
        })})
    }

    componentDidMount (){
        this.getAPI()
    }
   
    render() { 
        return ( 
            <div>
                <h2 className="dashboardstyle">Collaboration Bar</h2>
                <div className="row" >
                    {this.state.bars.map(bar => (
                        <Bars use={this.state.bar} key={bar.id} onClickMonth={this.Month} onClickYear={this.Year} bar={bar}  />
                    ))}
                </div> 
            </div> );
    }
}
 
export default CollaborationBar;