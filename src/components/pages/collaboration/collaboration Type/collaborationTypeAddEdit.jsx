import React, { Component } from 'react';

class CollaborationTypeAddEdit extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
                <div style={{display:"flex", justifyContent:"space-between"}}>
                    <div style={{display:"flex"}}>
                        <h1>
                            Collaboration Type<sub className="ubahColPadding">[Add New] or [Edit]</sub>
                        </h1>  
                    </div>
                    <div>
                        <button  className="btn btn-danger btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                            >Cancel
                        </button>
                        <button  className="btn btn-success btn-sm m-2" style={{ width: "100px", marginRight:"10px" }}
                            >Save
                        </button>
                    </div>
                </div>
                <div className=" col-xl-10 col-10 col-sm-10 col-md-10 col-lg-10 m-3 p-3" style={{backgroundColor:"white", borderTop:"#17a2b8 3px solid"}}>
                    <h6>Information</h6>
                    <hr/>
                    <h6 className="mb-3">Collaboration Type</h6>
                    <input className="mb-3 form-control" type="text" style={{width:"100%"}} placeholder="Enter the Collaboration Type"/>
                    <h6 className="mb-3">Collaboration Description</h6>
                    <textarea className="mb-3 form-control" type="textarea" style={{width:"100%"}} placeholder="Enter the Collaboration Description"/>
                </div>
            </div>
        );
    }
}
 
export default CollaborationTypeAddEdit;