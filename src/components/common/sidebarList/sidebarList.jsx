import React from "react";
import { Link } from "react-router-dom";
import "./sidebarList.css";

const SidebarList = props => {
  return ( 
    <Link className="designLink" to={props.sidebar.link} style={{color:"black"}}>
      <div className="row sidebar-list marginoff">
        <div className="col-3">
          <h6 className="middle" >
            <i className={"fa fa-" + props.sidebar.icon} />
          </h6>
        </div>
        <div className="col-9 " >
              <h6 style={{ fontSize:"1.3vw"}}>{props.sidebar.name}</h6>
        </div>
      </div>
    </Link>  
  );
};

export default SidebarList;
