import React from "react";
import "./card.css";

const Card = props => {
  return (
      <div className={"card bg-" + props.card.color} style={{ width: "18rem" }}>
        <div className="card-body">
          <div className="row card-detail">
            <div className="col-6">
              <h1 className="card-title">{props.card.total}</h1>
              <h6 className="card-subtitle mb-2">{props.card.name}</h6>
            </div>
            <div className="col-6">
              <h1 className="icon">
                <i className={"fa fa-" + props.card.icon} />
              </h1>
            </div>
          </div>
          <a href="#" className="card-link">
            More Info
          </a>
        </div>
      </div>
  );
};

export default Card;
