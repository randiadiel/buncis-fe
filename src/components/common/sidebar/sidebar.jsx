import React, { Component } from "react";
import SidebarList from "../sidebarList/sidebarList";
import  "./sidebar.css";

class Sidebar extends Component {
  render() {
    return (
      <div className="col-2 sidebarcol" style={{ display: this.props.status ,height:"100vh"}}>
        <div className="sidebar " style={{height:"100%"}}>
          <div className="navbar navbar-expand-lg navbar-light logoBuncis">
          <span className="navbar-toggler navmodif">
            BUNCIS
          </span>
            <span className="collapse navbar-collapse navbardif " id="navbarToggle1">
              BUNCIS
            </span>
          </div>
          <div className="" >
            {this.props.sidebars.map(sidebar => (
              <SidebarList key={sidebar.id} sidebar={sidebar} />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Sidebar;
